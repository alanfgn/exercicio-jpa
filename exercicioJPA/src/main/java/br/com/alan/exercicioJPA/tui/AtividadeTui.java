package br.com.alan.exercicioJPA.tui;

import br.com.alan.exercicioJPA.dao.AtividadeDao;
import br.com.alan.exercicioJPA.domain.Cidade;

public class AtividadeTui {

	private AtividadeDao dao = new AtividadeDao();

	// A
	public void listarClientesVendedorNome(String nome) {
		dao.listarClientesVendedorNome(nome).forEach(c -> {
			System.out.println("Nome: " + c.getNome());
			System.out.println("Endereço: ");
		});
	}

	// B
	public void listarVendedoresClientesRamoNome(String nome) {
		dao.listarVendedoresCidade(nome).forEach(v -> {

			System.out.println("Nome: " + v.getNome());
			System.out.println("Telefones: ");
			v.getTelefones().forEach(t -> System.out.println("\tNumero: " + t));
			System.out.println("Endereço: " + v.getEndereco());

		});
	}

	// C
	public void listarFuncionariosAtivos() {
		dao.listarFuncionariosAtivos().forEach(f -> {
			System.out.println("CPF: " + f.getCpf());
			System.out.println("Nome: " + f.getNome());
		});
	}

	// D
	public void listarEstadoSemCidade() {
		dao.listarEstadoSemCidade().forEach(e -> {
			System.out.println("Sigla: " + e.getSigla());
			System.out.println("Nome: " + e.getNome());
		});
	}

	// E
	public void listarQuantidadeVendedoresCidade() {
		dao.listarQuantidadeVendedoresCidade().forEach(cq -> {
			System.out.println("Nome: " + ((Cidade) cq[0]).getNome());
			System.out.println("Qtd: " + cq[1]);
		});
	}

	// F
	public void listarVendedoresCidade(String nome) {
		dao.listarVendedoresCidade(nome).forEach(v -> {
			System.out.println("Nome: " + v.getNome());
			System.out.println("Telefones: ");
			v.getTelefones().forEach(t -> System.out.println("\tNumero: " + t));
		});
	}
}
