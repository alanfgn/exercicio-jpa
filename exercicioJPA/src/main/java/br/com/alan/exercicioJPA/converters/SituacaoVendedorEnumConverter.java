package br.com.alan.exercicioJPA.converters;

import javax.persistence.AttributeConverter;

import br.com.alan.exercicioJPA.domain.SituacaoVendedorEnum;

public class SituacaoVendedorEnumConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum situacao) {
		return situacao.getSigla();
	}

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String situacao) {
		return SituacaoVendedorEnum.valueOfSigla(situacao);
	}

}
