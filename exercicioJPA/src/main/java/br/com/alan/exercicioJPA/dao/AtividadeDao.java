package br.com.alan.exercicioJPA.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.alan.exercicioJPA.domain.Estado;
import br.com.alan.exercicioJPA.domain.Funcionario;
import br.com.alan.exercicioJPA.domain.PessoaJuridica;
import br.com.alan.exercicioJPA.domain.SituacaoVendedorEnum;
import br.com.alan.exercicioJPA.domain.Vendedor;

public class AtividadeDao {

	private EntityManager em = JpaUtil.getEntityManager();

	// A
	public List<PessoaJuridica> listarClientesVendedorNome(String nome) {
		return em.createQuery("select p from PessoaJuridica p join p.vendedores v where v.nome = :pNome",
				PessoaJuridica.class).setParameter("pNome", nome).getResultList();
	}

	// B
	public List<Vendedor> listarVendedoresClientesRamoNome(String nome) {
		return em.createQuery(
				"select v from Vendedor v join fetch v.telefones join v.clientes c join c.ramos r where r.nome = :pNome",
				Vendedor.class).setParameter("pNome", nome).getResultList();
	}

	// C
	public List<Funcionario> listarFuncionariosAtivos() {
		return em.createQuery("select f from Funcionario f where treat(f as Vendedor).situacao = :pSituacao",
				Funcionario.class).setParameter("pSituacao", SituacaoVendedorEnum.ATIVO).getResultList();
	}

	// D
	public List<Estado> listarEstadoSemCidade() {
		return em.createQuery("select e from Estado e left join e.cidades c where c is null", Estado.class)
				.getResultList();
	}

	// E
	public List<Object[]> listarQuantidadeVendedoresCidade() {
		return em.createQuery("select c, count(v) from Vendedor v join v.endereco.cidade c group by c", Object[].class)
				.getResultList();
	}
	
	// F
	public List<Vendedor> listarVendedoresCidade(String nome) {		
		return em.createQuery("select v from Vendedor v join fetch v.telefones join v.endereco.cidade c where c.nome = :pNome", Vendedor.class)
				.setParameter("pNome", nome)
				.getResultList();
	}

}
