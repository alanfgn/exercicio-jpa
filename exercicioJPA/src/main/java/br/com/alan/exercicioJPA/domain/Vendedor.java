package br.com.alan.exercicioJPA.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.alan.exercicioJPA.converters.SituacaoVendedorEnumConverter;

@Entity
@PrimaryKeyJoinColumn(name = "ven_fun_cpf", 
	referencedColumnName = "fun_cpf", 
	foreignKey = @ForeignKey(name = "fk_ven_funcionario"), 
	columnDefinition = "char(11)")
@Table(name = "tab_vendedores")
public class Vendedor extends Funcionario {

	@Column(name = "ven_percentual_contribuicao", scale = 10, precision = 2, columnDefinition = "numeric(10,2)", nullable = false)
	private Double percentualContribuicao;

	@Convert(converter = SituacaoVendedorEnumConverter.class)
	@Column(name = "ven_situacao_vendedor", columnDefinition = "char(3)", nullable = false)
	private SituacaoVendedorEnum situacao;

	@ManyToMany
	@JoinTable(name = "tab_pessoas_juridicas_vendedores", 
		joinColumns = @JoinColumn(name = "pjv_ven_fun_cpf", referencedColumnName = "ven_fun_cpf", 
			columnDefinition = "char(11)",foreignKey = @ForeignKey(name = "fk_pjv_vendedores")), 
		inverseJoinColumns = @JoinColumn(name = "pjv_psj_cnpj", referencedColumnName = "psj_cnpj", 
			columnDefinition = "char(11)" ,foreignKey = @ForeignKey(name = "fk_pjv_pessoas_juridicas")))
	private List<PessoaJuridica> clientes;
	
	public Vendedor() {
		super();
	}
	
	public Vendedor(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereco, Double percentualContribuicao, SituacaoVendedorEnum situacao,
			List<PessoaJuridica> clientes) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.percentualContribuicao = percentualContribuicao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public Double getPercentualContribuicao() {
		return percentualContribuicao;
	}

	public void setPercentualContribuicao(Double percentualContribuicao) {
		this.percentualContribuicao = percentualContribuicao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}
	
	public void addCliente(PessoaJuridica cliente) {
		this.clientes.add(cliente);
	}

}
