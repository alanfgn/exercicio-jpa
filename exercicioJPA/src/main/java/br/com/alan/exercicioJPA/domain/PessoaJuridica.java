package br.com.alan.exercicioJPA.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoas_juridicas")
public class PessoaJuridica {

	@Id
	@Column(name = "psj_cnpj", columnDefinition = "char(11)")
	private String cnpj;

	@Column(name = "psj_nome", length = 40, nullable = false)
	private String nome;

	@ManyToMany
	@JoinTable(name = "tab_pessoas_juridicas_ramos", 
		joinColumns = @JoinColumn(name = "pjr_psj_cnpj", 
		referencedColumnName = "psj_cnpj", columnDefinition = "char(11)", 
			foreignKey = @ForeignKey(name = "fk_pjr_pessoas_juridicas")), 
		inverseJoinColumns = @JoinColumn(name = "pjr_rma_id", referencedColumnName = "rma_id", 
			foreignKey = @ForeignKey(name = "fk_pjr_ramos_atividades")))
	private List<RamoAtividade> ramos;

	@Column(name = "psj_faturamento", scale = 10, precision = 2, columnDefinition = "numeric(10,2)", nullable = false)
	private Double faturamento;

	@ManyToMany(mappedBy = "clientes")
	private List<Vendedor> vendedores;

	public PessoaJuridica() {
		super();
	}
	
	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramos, Double faturamento) {
		this(cnpj, nome, ramos, faturamento, new ArrayList<>());
	}

	
	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramos, Double faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramos = ramos;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamos() {
		return ramos;
	}

	public void setRamos(List<RamoAtividade> ramos) {
		this.ramos = ramos;
	}
	
	public void addRamo(RamoAtividade ramo) {
		this.ramos.add(ramo);
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}
	
	public void addVendedor(Vendedor vendedor) {
		this.vendedores.add(vendedor);
	}

}
