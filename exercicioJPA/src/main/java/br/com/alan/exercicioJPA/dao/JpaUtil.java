package br.com.alan.exercicioJPA.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	
	private static final String UNIT = "exercicioJPA";
	
	private static EntityManagerFactory entityManagerFactory 
		= Persistence.createEntityManagerFactory(UNIT);

	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
}
