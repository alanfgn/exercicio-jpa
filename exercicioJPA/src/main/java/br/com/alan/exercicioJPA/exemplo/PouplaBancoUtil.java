package br.com.alan.exercicioJPA.exemplo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.alan.exercicioJPA.domain.Administrativo;
import br.com.alan.exercicioJPA.domain.Cidade;
import br.com.alan.exercicioJPA.domain.Endereco;
import br.com.alan.exercicioJPA.domain.Estado;
import br.com.alan.exercicioJPA.domain.PessoaJuridica;
import br.com.alan.exercicioJPA.domain.RamoAtividade;
import br.com.alan.exercicioJPA.domain.SituacaoVendedorEnum;
import br.com.alan.exercicioJPA.domain.Vendedor;

public class PouplaBancoUtil {

	public static void popular() {

		EntityManager em = Persistence.createEntityManagerFactory("exercicioJPA").createEntityManager();

		em.getTransaction().begin();

		Estado bahia 		= new Estado("BA", "Bahia");
		Estado saoPaulo 	= new Estado("SP", "São Paulo");
		Estado rioDeJaneiro = new Estado("RJ", "Rio de Janeiro");

		em.persist(bahia);
		em.persist(saoPaulo);
		em.persist(rioDeJaneiro);
		
		Cidade salvador 			= new Cidade("ssa", "Salvador", bahia);
		Cidade vitoriaDaConqusita 	= new Cidade("vic", "Vitória da Conquista", bahia);
		Cidade feiraDeSantana 		= new Cidade("fsa", "Feira de Santana", bahia);

		Cidade cidadeSaoPaulo 		= new Cidade("sap", "São Paulo", saoPaulo);
		Cidade saoBernadoDoCampo 	= new Cidade("sbc", "São Bernado do Campo", saoPaulo);

		em.persist(salvador);
		em.persist(vitoriaDaConqusita);
		em.persist(feiraDeSantana);

		em.persist(cidadeSaoPaulo);
		em.persist(saoBernadoDoCampo);

		RamoAtividade tecnologia 	= new RamoAtividade("Tecnologia");
		RamoAtividade gastronomia 	= new RamoAtividade("Gastronomia");
		RamoAtividade engenharia 	= new RamoAtividade("Engenharia");

		em.persist(tecnologia);
		em.persist(gastronomia);
		em.persist(engenharia);
				
		PessoaJuridica techEng 			= new PessoaJuridica
				("11111111111", "Tech Eng", 	Arrays.asList(tecnologia, engenharia), 36000d);
		PessoaJuridica civis 			= new PessoaJuridica
				("22222222222", "Civis", 		Arrays.asList(engenharia), 20000d);
		PessoaJuridica treechonologia 	= new PessoaJuridica
				("33333333333", "3chonologia", 	Arrays.asList(tecnologia),55000d);
		PessoaJuridica enGenieiros 		= new PessoaJuridica
				("44444444444", "Engenieiros",	Arrays.asList(engenharia),22000d);
		PessoaJuridica fabSoft 			= new PessoaJuridica
				("55555555555", "FabSofs", 		Arrays.asList(tecnologia, engenharia), 65000d);

		em.persist(techEng);
		em.persist(civis);
		em.persist(treechonologia);
		em.persist(enGenieiros);
		em.persist(fabSoft);

		Vendedor luciano = new Vendedor("66666666666", "Luciano", "06", "SSP", "BA", Arrays.asList("666666666666"),
				new Date(1996, 6, 6), new Endereco("Rua L", "Bairro Salvador", salvador), 20.2d,
				SituacaoVendedorEnum.ATIVO, Arrays.asList(techEng, treechonologia));

		Vendedor alice = new Vendedor("77777777777", "Alice", "07", "SSP", "SP",
				Arrays.asList("77777777777", "77777777778"), new Date(1997, 7, 7),
				new Endereco("Rua A", "Bairro SP", cidadeSaoPaulo), 10.2d, SituacaoVendedorEnum.ATIVO,
				Arrays.asList(fabSoft));

		Vendedor bob = new Vendedor("88888888888", "Bob", "08", "SSP", "BA", Arrays.asList("88888888888"),
				new Date(1988, 8, 8), new Endereco("Rua B", "Bairro SSA", salvador), 5.5d,
				SituacaoVendedorEnum.SUSPENSO, new ArrayList<>());

		Vendedor denis = new Vendedor("99999999999", "Denis", "09", "SSP", "BA", Arrays.asList("99999999999"),
				new Date(1996, 6, 6), new Endereco("Rua D", "Bairro Vitoria da conquista", vitoriaDaConqusita), 50.2d,
				SituacaoVendedorEnum.ATIVO, Arrays.asList(techEng, civis));

		Vendedor caio = new Vendedor("10101010101", "Caio", "10", "SSP", "SP",
				Arrays.asList("101010101010", "010101010101"), new Date(1990, 10, 10),
				new Endereco("Rua C", "Bairro Sao Bernado", saoBernadoDoCampo), 20.2d, SituacaoVendedorEnum.ATIVO,
				Arrays.asList(civis));

		em.persist(luciano);
		em.persist(alice);
		em.persist(bob);
		em.persist(denis);
		em.persist(caio);
		
		Administrativo ernesto = new Administrativo("11011011011", "Ernesto", "11", "SSP", "BA",
				Arrays.asList("110110110110"), new Date(1991, 11, 11),
				new Endereco("Rua E", "Bairro Soteropolis", salvador), 11);

		Administrativo fausto = new Administrativo("12012012012", "Fausto", "12", "SSP", "SP",
				Arrays.asList("120120120120"), new Date(1992, 12, 12),
				new Endereco("Rua F", "Bairro Sampa", cidadeSaoPaulo), 12);

		Administrativo gabriel = new Administrativo("13013013013", "Gabriel", "13", "SSP", "BA",
				Arrays.asList("130130130130"), new Date(1991, 3, 13),
				new Endereco("Rua G", "Bairro Feira de Santana", feiraDeSantana), 13);

		em.persist(ernesto);
		em.persist(fausto);
		em.persist(gabriel);

		em.getTransaction().commit();

	}
}
