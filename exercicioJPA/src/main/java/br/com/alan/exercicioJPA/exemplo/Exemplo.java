package br.com.alan.exercicioJPA.exemplo;

import br.com.alan.exercicioJPA.tui.AtividadeTui;

public class Exemplo {
	
	public static void main(String[] args) {
		PouplaBancoUtil.popular();										// Q1
		AtividadeTui atividadeTui = new AtividadeTui();					// Q2
		atividadeTui.listarClientesVendedorNome("Alice");				// A
		atividadeTui.listarVendedoresClientesRamoNome("Tecnologia");	// B
		atividadeTui.listarFuncionariosAtivos();						// C
		atividadeTui.listarEstadoSemCidade();							// D
		atividadeTui.listarQuantidadeVendedoresCidade();				// E
		atividadeTui.listarVendedoresCidade("Salvador");				// F
	}
}
