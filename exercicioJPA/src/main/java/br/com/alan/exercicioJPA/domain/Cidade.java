package br.com.alan.exercicioJPA.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidades")
public class Cidade {

	@Id
	@Column(name = "cid_sigla", columnDefinition = "char(3)")
	private String sigla;

	@Column(name = "cid_nome", length = 40, nullable = false)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "cid_est_sigla" ,referencedColumnName = "est_sigla", columnDefinition = "char(2)", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_cid_estado"))
	private Estado estado;
	
	public Cidade() {
		super();
	}
		
	public Cidade(String sigla, String nome, Estado estado) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
