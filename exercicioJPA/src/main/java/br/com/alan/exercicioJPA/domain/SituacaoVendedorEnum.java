package br.com.alan.exercicioJPA.domain;

public enum SituacaoVendedorEnum {
	ATIVO("ATV"), SUSPENSO("SPN");
	
	private String sigla;
	
	private SituacaoVendedorEnum(String sigla) {
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}

	public static SituacaoVendedorEnum valueOfSigla(String sigla) {
		for(SituacaoVendedorEnum situacao : values()) {
				if(situacao.getSigla().equalsIgnoreCase(sigla)) 
					return situacao;
		}
		throw new IllegalArgumentException();
	}
	
}
